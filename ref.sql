-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.23 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table ref.country
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table ref.country: ~4 rows (approximately)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`id`, `name`) VALUES
	(1, '-'),
	(2, 'New Zealand'),
	(3, 'Brazil'),
	(4, 'Kazakhstan');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;


-- Dumping structure for table ref.gender
CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table ref.gender: ~2 rows (approximately)
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` (`id`, `name`) VALUES
	(1, '-'),
	(2, 'Male'),
	(3, 'Female');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;


-- Dumping structure for table ref.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ref.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Dumping structure for table ref.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ref.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


-- Dumping structure for table ref.reference
CREATE TABLE IF NOT EXISTS `reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `who_asks_user_id` int(11) DEFAULT NULL,
  `who_gives_user_id` int(11) DEFAULT NULL,
  `reftype_id` int(11) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `date_of_ask` date DEFAULT NULL,
  `date_of_reference` date DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `approved_tf` int(11) DEFAULT NULL,
  `reference_notes` varchar(50) DEFAULT NULL,
  `rank_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table ref.reference: ~15 rows (approximately)
/*!40000 ALTER TABLE `reference` DISABLE KEYS */;
INSERT INTO `reference` (`id`, `who_asks_user_id`, `who_gives_user_id`, `reftype_id`, `position`, `date_of_ask`, `date_of_reference`, `status_id`, `approved_tf`, `reference_notes`, `rank_id`, `updated_at`, `created_at`) VALUES
	(1, 2, 3, 3, 'Junior Software Developer', NULL, '2018-12-04', 2, 1, 'Flexible in tech', 4, '2018-12-04 22:20:41', '2018-12-04 20:02:14'),
	(2, 3, 2, 4, NULL, '2018-12-04', NULL, 2, 1, 'Tidy', 4, '2018-12-04 22:15:21', '2018-12-04 20:06:02'),
	(3, 3, 2, 2, NULL, '2018-12-04', NULL, 2, 1, 'So So, more than less', 4, '2018-12-05 00:24:29', '2018-12-04 20:09:12'),
	(4, 4, 2, 3, 'Full stack', '2018-12-05', NULL, 2, 1, 'A lot of experoence', 4, '2018-12-05 20:35:19', '2018-12-05 20:33:07'),
	(5, 5, 6, 2, NULL, NULL, '2018-12-06', 2, 1, 'Good looking person. Nice woman', 5, '2018-12-06 10:39:06', '2018-12-06 10:36:44'),
	(6, 6, 5, 4, NULL, '2018-12-06', NULL, 2, 1, 'TidyMidy', 4, '2018-12-06 10:39:14', '2018-12-06 10:37:32'),
	(7, 5, 2, 3, 'Technician', NULL, '2018-12-06', 2, 0, 'The best Revit draftsman', 5, '2018-12-06 10:40:52', '2018-12-06 10:40:52'),
	(8, 5, 3, 4, NULL, NULL, '2018-12-06', 2, 0, 'Good', 5, '2018-12-06 19:38:13', '2018-12-06 19:38:13'),
	(9, 5, 5, 4, NULL, '2018-12-06', NULL, 1, 0, NULL, NULL, '2018-12-06 19:39:21', '2018-12-06 19:39:21'),
	(10, 5, 2, 4, NULL, NULL, '2018-12-06', 2, 0, 'www', 5, '2018-12-06 21:21:24', '2018-12-06 21:21:24'),
	(11, 7, 3, 3, 'softwara', NULL, '2018-12-07', 2, 1, 'gbyybybbybyby', 4, '2018-12-07 22:27:57', '2018-12-07 22:23:06'),
	(12, 3, 7, 4, NULL, '2018-12-07', NULL, 2, 0, 'tidy', 5, '2018-12-07 22:32:39', '2018-12-07 22:32:05'),
	(13, 8, 3, 3, 'Software engineer', NULL, '2018-12-12', 2, 0, 'Have a good background skiils', 5, '2018-12-12 01:27:53', '2018-12-12 01:27:53'),
	(14, 8, 2, 4, NULL, NULL, '2018-12-12', 2, 0, 'Very tidy guy', 5, '2018-12-12 01:28:50', '2018-12-12 01:28:50'),
	(15, 8, 5, 2, NULL, NULL, '2018-12-12', 2, 0, 'Lasantha is a Gentleman', 5, '2018-12-12 01:30:12', '2018-12-12 01:30:12');
/*!40000 ALTER TABLE `reference` ENABLE KEYS */;


-- Dumping structure for table ref.reftype
CREATE TABLE IF NOT EXISTS `reftype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table ref.reftype: ~4 rows (approximately)
/*!40000 ALTER TABLE `reftype` DISABLE KEYS */;
INSERT INTO `reftype` (`id`, `name`) VALUES
	(1, '-'),
	(2, 'Personal attraction'),
	(3, 'Job'),
	(4, 'Rent');
/*!40000 ALTER TABLE `reftype` ENABLE KEYS */;


-- Dumping structure for table ref.status
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table ref.status: ~2 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`id`, `name`) VALUES
	(1, 'Asked'),
	(2, 'Given');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;


-- Dumping structure for table ref.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL DEFAULT '0',
  `gender_id` int(11) NOT NULL DEFAULT '0',
  `visa_id` int(11) NOT NULL DEFAULT '0',
  `experience` int(11) NOT NULL DEFAULT '0',
  `date_of_birth` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ref.users: ~7 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `phone`, `password`, `city`, `country_id`, `admin`, `gender_id`, `visa_id`, `experience`, `date_of_birth`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'AlexeyAbramov', 'a@gmail.com', NULL, '1', '$2y$10$5qTgmGlco8bE7umlJSI0fOpJ.FNa6nwnn6118weIJhQ9vqDLqPJ4a', 'Ust-Kamenogorsk', 4, 1, 2, 2, 1, NULL, 'bPGUe0iwxpDx9br1NbkfodDMPmf7fD0GvmHaxZ9fqHLV7craScq30lBTxnAP', '2018-12-04 19:58:12', '2018-12-04 19:58:12'),
	(2, 'Gabriela Andrade de Araujo', 'g@gmail.com', NULL, '2', '$2y$10$SciFkX4MdcsFwNyx8akHyehqoZH6hM4YaCcgZrQwSYC5VK6pjcq2i', 'S', 3, 0, 3, 4, 0, NULL, 'UaHX3D2BIbaueYPMylcEu2DYIl4Kym2W4AtXIklYAXkTVkBGEj63r9UV7SNs', '2018-12-04 19:59:03', '2018-12-04 19:59:03'),
	(3, 'Lucas Dias Mota', 'l@gmail.com', NULL, '3', '$2y$10$QQe0LHemo8rQaIyaYsgzTup/2fLV0/s2dyHi7hON0480rHy8/nYtW', 'Sao Paulo', 3, 0, 2, 4, 1, NULL, 'ArtIcNYkDZGoYL2cD7gN87IHTAmGTnjH5nXvUkLIGnbJ4ttH8bffLKNkuJGo', '2018-12-04 22:34:22', '2018-12-04 22:34:22'),
	(4, 'Dipendra', 'd@gmail.com', NULL, '1', '$2y$10$yYcsxBA33r0xQBLbiRpwH.6K0h23x.WJO.raih5jtkxHhZJP57DGq', 'ChCh', 2, 0, 2, 5, 1, NULL, 'NWSEemdklU7mQOACSitc5LcqomSg0u7ee6yHKf0zoJ2Gxx2UQXM1Y8x5dWmZ', '2018-12-05 20:32:11', '2018-12-05 20:32:11'),
	(5, 'Irina', 'i@gmail.com', NULL, '3', '$2y$10$7JizR.qRp10XCHNtH9ZsUOl.BnfXXBaCXNCHgtb9jl3kdthfMY1Oi', 'Christchurch', 2, 0, 3, 4, 1, NULL, 'AE8aTkmcAo0LwlxI6WVS4isQp1mNLKxcHJiXSr5vTucIOxmlWcvHUY0m2Dfn', '2018-12-06 10:34:39', '2018-12-06 10:34:39'),
	(6, 'Mykola', 'm@gmail.com', NULL, '6', '$2y$10$CJlzxKenXcIZh28azArKQe/dX2K9OTXcCuuNfRM0W/59tt54ahT.q', 'Christchurch', 2, 0, 2, 5, 1, NULL, '0Vr12yYd7kcLF4Z45eIFxw9nLtK3RSFsPh7T891koT3kUxSfppQY56TJlPz4', '2018-12-06 10:36:03', '2018-12-06 10:36:03'),
	(7, 'Andrew', 'andrew@gmail.com', NULL, '8888888', '$2y$10$6XyfMwolbgUYfHYk17CtBOUoHnDyrhWjhhxGf2o.wNhYaWeYw4wde', 'ChCh', 2, 0, 2, 5, 0, NULL, '5Adz5JXMHIrdR2NdRad8KKkCizFo3j9IgmDXlxxLYtlzV2wnUwGoQbWjZUtR', '2018-12-07 22:16:24', '2018-12-07 22:16:24'),
	(8, 'AA Lasantha Perera', 'aalgperera@gmail.com', NULL, '0226790976', '$2y$10$ZWZoOsP73nxvdkPwa6JjzOkvsoTZIq.CPOd/KCbpXLBU14e/ti3W2', 'Christchurch', 2, 0, 2, 5, 1, NULL, 'qrZLAYKzNscqaPHqq5Drxjq5t5005ujT3tTHd0JHp6TSFpBrWv4rOAnEMBD3', '2018-12-12 01:25:43', '2018-12-12 01:25:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table ref.visa
CREATE TABLE IF NOT EXISTS `visa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table ref.visa: ~4 rows (approximately)
/*!40000 ALTER TABLE `visa` DISABLE KEYS */;
INSERT INTO `visa` (`id`, `name`) VALUES
	(1, '-'),
	(2, 'Student'),
	(3, 'Open Work Visa'),
	(4, 'Work Visa'),
	(5, 'Resident');
/*!40000 ALTER TABLE `visa` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
