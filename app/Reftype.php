<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reftype extends Model
{
    protected $table = 'Reftype';
    public $primaryKey = 'id';
    public $timestamps = true;
}
