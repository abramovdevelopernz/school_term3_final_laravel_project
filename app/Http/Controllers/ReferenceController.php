<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Reftype;
use App\Reference;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;

use Auth;


class ReferenceController extends Controller
{

    public function searchbyid(Request $request)
    {
        return view('search.searchbyid');
    }

    public function searchbyqr_ask()
    {
        return view('search.searchbyqr_ask') ;
    }

    public function searchbyqr()
    {
        return view('search.searchbyqr') ;
    }

    public function option()
    {
        return view('search.option') ;
    }

    public function option_ask()
    {
        return view('search.option_ask') ;
    }

    public function searchbyid_ask(Request $request)
    {
        return view('search.searchbyid_ask');
    }

    public function answer_update(Request $request, $id)
    {

        $this->validate($request,[
            'reference_notes'=>'required|string|max:255',
        ]);

        $reference_id=$request->input('reference_id');
        $reference = Reference::find($reference_id);
        $reference->reference_notes = $request->input('reference_notes');
        $reference->rank_id = $request->input('rank_id');
        $reference->status_id = 2;
        $reference->save();
        return redirect('/home')->with('success','Reference given ');
    }

    public function answer($id,$reference_id)
    {

        $data = array(
            'id'=>$id,
            'reference_id'=>$reference_id,
        );

        return view('reference.answer')->with($data) ;
    }

    public function approve ($id)
    {
        $reference = Reference::find($id);
        $reference->approved_tf = 1;
        $reference->save();
        return redirect('/home')->with('success','Reference approved ');
    }

    public function result_searchbyid_ask(Request $request, $id)
    {

        $person_id = Auth::user()->id;

        $user = DB::select('
        select 
        users.id,
        users.name,
        users.email,
        users.phone,
        users.city,
        users.experience,
        country.name as country_name,
        gender.name as gender_name,
        visa.name as visa_name 
        from 
        users, 
        gender, 
        country, 
        visa 
        where 
        users.gender_id=gender.id and 
        users.visa_id=visa.id and 
        users.country_id = country.id and 
        users.id = '.$request -> input('searchid') );

        $data = array(
            'person_id'=>$person_id,
            'user'=>$user
        );

        return view('search.result_searchbyid_ask')->with($data) ;

    }

    public function ask($id)
    {
       // $user = auth()->user();
      //  $user_id = $user->id;

        $user_id = Auth::user()->id;

        $user = DB::select('
        select 
        users.id,
        users.name,
        users.email,
        users.phone,
        users.gender_id,
        users.city,
        users.experience,
        country.name as country_name,
        gender.name as gender_name,
        visa.name as visa_name 
        from 
        users, 
        gender, 
        country, 
        visa 
        where 
        users.gender_id=gender.id and 
        users.visa_id=visa.id and 
        users.country_id = country.id and 
        users.id = '.$id);

        $Reftype = Reftype::all();
        $data = array(
            'id'=>$id,
            'Reftype'=>$Reftype,
            'user_id'=>$user_id,
            'user'=>$user
        );

        return view('reference.ask')->with($data) ;
    }

    public function createrecord_ask(Request $request,$id)
    {
        $reference = new Reference;
        $reference->who_gives_user_id = $request -> input('who_gives_user_id');
        $reference->who_asks_user_id = $request -> input('who_asks_user_id');
        $reference->reftype_id = $request -> input('reftype_id');
        $reference->position = $request -> input('position');
        $reference->date_of_ask = date("Y-m-d");
        $reference->date_of_reference = NULL;
        $reference->status_id = 1;
        $reference->approved_tf = 0;
        $reference ->save();
        return redirect('/home')->with('success','Reference asked');
    }

    public function result_searchbyid1_ask($id)
    {

      //  $user = auth()->user();
     //   $person_id = $user->id;
        $person_id = Auth::user()->id;


        $user = DB::select('
        select 
        users.id,
        users.name,
        users.email,
        users.phone,
        users.city,
        users.gender_id,
        users.experience,
        country.name as country_name,
        gender.name as gender_name,
        visa.name as visa_name 
        from 
        users, 
        gender, 
        country, 
        visa 
        where 
        users.gender_id=gender.id and 
        users.visa_id=visa.id and 
        users.country_id = country.id and 
        users.id = '.$id);

        $data = array(
            'person_id'=>$person_id,
            'user'=>$user
        );

        return view('search.result_searchbyid_ask')->with($data) ;

    }

    public function myreferences($id)
    {

        $reference = DB::select('
        select 
        reference.id,
        users.name as who_name,
        users.gender_id,
        reference.position,
        reference.date_of_ask,
        reference.date_of_reference,
        reference.approved_tf,
        reference.reference_notes,
        reference.rank_id,       
        reftype.name as reftype,
        status.name as status       
        from 
        reference, 
        reftype, 
        status,        
        users
        where 
        users.id=reference.who_gives_user_id and 
        reference.reftype_id = reftype.id and 
        reference.status_id = status.id and 
        reference.who_asks_user_id ='.$id);

        $data = array(
            'reference'=>$reference
        );

        return view('reference.myreferences')->with($data) ;

    }

    public function someonereferences($id)
    {

        $reference = DB::select('
        select 
        reference.id,
        users.name as who_name,
        users.gender_id,
        reference.position,
        reference.date_of_ask,
        reference.date_of_reference,
        reference.approved_tf,
        reference.reference_notes,
        reference.rank_id,       
        reftype.name as reftype,
        status.name as status       
        from 
        reference, 
        reftype, 
        status,        
        users
        where 
        users.id=reference.who_asks_user_id and 
        reference.reftype_id = reftype.id and 
        reference.status_id = status.id and 
        reference.who_gives_user_id ='.$id);

        $data = array(
            'reference'=>$reference,
            'reference_id'=>$id
        );

        return view('reference.someonereferences')->with($data) ;

    }

    public function approval()
    {

        $reference = DB::select('
        select 
        reference.id,
        user_ask.name as who_ask_name,
        user_give.name as who_give_name,
        user_ask.gender_id,
        reference.position,
        reference.date_of_ask,
        reference.date_of_reference,
        reference.approved_tf,
        reference.reference_notes,
        reference.rank_id,       
        reftype.name as reftype,
        status.name as status       
        from 
        reference, 
        reftype, 
        status,        
        users user_ask,
        users user_give
        where 
        user_ask.id=reference.who_asks_user_id and 
        user_give.id=reference.who_gives_user_id and 
        reference.reftype_id = reftype.id and 
        reference.status_id = status.id ');

        $data = array(
            'reference'=>$reference,

        );

        return view('reference.approval')->with($data) ;

    }

    public function createrecord(Request $request,$id)
    {

        $this->validate($request,[
            'reference_notes'=>'required|string|max:255',
        ]);

        $reference = new Reference;
        $reference->who_gives_user_id = $request -> input('who_gives_user_id');
        $reference->who_asks_user_id = $request -> input('who_asks_user_id');
        $reference->reftype_id = $request -> input('reftype_id');
        $reference->position = $request -> input('position');
        $reference->date_of_ask = NULL;
        $reference->date_of_reference = date("Y-m-d");
        $reference->status_id = 2;
        $reference->approved_tf = 0;
        $reference->reference_notes = $request -> input('reference_notes');
        $reference->rank_id = $request -> input('rank_id');
        $reference ->save();
        return redirect('/home')->with('success','Reference created');
    }

    public function result_searchbyid(Request $request, $id)
    {

    $person_id = Auth::user()->id;

    $user = DB::select('
    select 
    users.id,
    users.name,
    users.email,
    users.phone,
    users.city,
    users.experience,
    country.name as country_name,
    gender.name as gender_name,
    visa.name as visa_name 
    from 
    users, 
    gender, 
    country, 
    visa 
    where 
    users.gender_id=gender.id and 
    users.visa_id=visa.id and 
    users.country_id = country.id and 
    users.id = '.$request -> input('searchid') );

    $data = array(
        'person_id'=>$person_id,
        'user'=>$user
    );

        return view('search.result_searchbyid')->with($data) ;

    }

    public function result_searchbyid1($id)
    {
        $person_id = Auth::user()->id;

        $user = DB::select('
        select 
        users.id,
        users.name,
        users.email,
        users.phone,
        users.city,
        users.experience,
        country.name as country_name,
        gender.name as gender_name,
        visa.name as visa_name 
        from 
        users, 
        gender, 
        country, 
        visa 
        where 
        users.gender_id=gender.id and 
        users.visa_id=visa.id and 
        users.country_id = country.id and 
        users.id = '.$id);

        $data = array(
            'person_id'=>$person_id,
            'user'=>$user
        );

        return view('search.result_searchbyid')->with($data) ;

    }

    public function provide($id)
    {
       // $user = auth()->user();
      //  $user_id = $user->id;

        $user_id = Auth::user()->id;

        $user = DB::select('
        select 
        users.id,
        users.name,
        users.email,
        users.phone,
        users.city,
        users.gender_id,
        users.experience,
        country.name as country_name,
        gender.name as gender_name,
        visa.name as visa_name 
        from 
        users, 
        gender, 
        country, 
        visa 
        where 
        users.gender_id=gender.id and 
        users.visa_id=visa.id and 
        users.country_id = country.id and 
        users.id = '.$id);

        $Reftype = Reftype::all();
        $data = array(
            'id'=>$id,
            'Reftype'=>$Reftype,
            'user_id'=>$user_id,
            'user'=>$user
        );

        return view('reference.provide')->with($data) ;
    }

    public function cv($name)
    {

        $reference = DB::select('
        select 
        reference.id,
        user_ask.name as who_ask_name,
        user_give.name as who_give_name,
        user_ask.gender_id,
        user_ask.email as who_ask_name_email,
        reference.position,
        reference.date_of_ask,
        reference.date_of_reference,
        reference.approved_tf,
        reference.reference_notes,
        reference.rank_id,       
        reftype.name as reftype,
        status.name as status       
        from 
        reference, 
        reftype, 
        status,        
        users user_ask,
        users user_give
        where 
        user_ask.id=reference.who_asks_user_id and 
        user_give.id=reference.who_gives_user_id and 
        reference.reftype_id = reftype.id and 
        reference.status_id = status.id and 
        user_ask.email ="'.$name.'"');

        $data = array(
            'reference'=>$reference,
        );

        return view('cv')->with($data) ;

    }


    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }

    public function index()
    {

    }
}
