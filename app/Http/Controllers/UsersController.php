<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\Gender;
use App\Visa;
use DB;

class UsersController extends Controller
{

    public function show($id)
    {

    $user = DB::select('
    select 
    users.name,
    users.email,
    users.phone,
    users.city,
    users.experience,
    country.name as country_name,
    gender.name as gender_name,
    visa.name as visa_name 
    from 
    users, 
    gender, 
    country, 
    visa 
    where 
    users.gender_id=gender.id and 
    users.visa_id=visa.id and 
    users.country_id = country.id and 
    users.id = '.$id );

    return view('users.show', ['user' => $user]);

    }

    public function edit($id)
    {

        $Country = Country::all();
        $Gender = Gender::all();
        $Visa = Visa::all();

        $data = array(
            'Country'=>$Country,
            'Visa'=>$Visa,
            'Gender'=>$Gender
        );

        $user = User::find($id);

        return view('users.edit', ['user' => $user])->with($data);

    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255',
            'phone'=>'required|string|max:255',
            'city'=>'required|string|max:255',
        ]);

        //            'email'=>'required|string|email|max:255|unique:users',

       $experience = $request->input('experience');
       $experience = (isset($experience) =='1' ? '1' : '0');

         //   'password' => 'required|string|min:6|confirmed',

        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->country_id = $request->input('country_id');
        $user->city = $request->input('city');
        $user->visa_id = $request->input('visa_id');
        $user->gender_id = $request->input('gender_id');
        $user->experience = $experience;
        $user->save();

        return redirect('/home')->with('success','User Updated');
       // return redirect('/home');
    }

    public function destroy($id)
    {

    }

    public function index()
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }
}
