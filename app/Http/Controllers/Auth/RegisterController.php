<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Country;
use App\Gender;
use App\Visa;

//use Illuminate\Support\Facades\DB;


class RegisterController extends Controller
{
    use RegistersUsers;


    protected $redirectTo = '/home';


    public function showRegistrationForm()
    {
        $Country = Country::all();
        $Gender = Gender::all();
        $Visa = Visa::all();

        $data = array(
            'Country'=>$Country,
            'Visa'=>$Visa,
            'Gender'=>$Gender,

        );

        //dd($data);

        return view('auth.register')->with($data);
    }



    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users',
            'phone' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'password' => 'required|string|min:1|confirmed'
        ]);

    }

    protected function create(array $data)
    {
        $experience = (isset($data['experience']) =='1' ? '1' : '0');
      //  dd($data);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'country_id' => $data['country_id'],
            'gender_id' => $data['gender_id'],
            'visa_id' => $data['visa_id'],
            'experience' => $experience,
            'password' => Hash::make($data['password'])
        ]);

    }
}
