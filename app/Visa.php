<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{
    protected $table = 'Visa';
    public $primaryKey = 'id';
    public $timestamps = true;
}
