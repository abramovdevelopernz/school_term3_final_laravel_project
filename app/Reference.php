<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $table = 'Reference';
    public $primaryKey = 'id';
    public $timestamps = true;
}
