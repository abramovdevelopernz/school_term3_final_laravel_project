<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'Gender';
    public $primaryKey = 'id';
    public $timestamps = true;
}
