<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/
use App\Country;


Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
 //   Artisan::call('backup:clean');
    print_r('Clear');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::resource('country','CountryController');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/user/show/{id}', 'UsersController@show');
Route::get('/user/edit/{id}', 'UsersController@edit');
Route::post('/user/update/{id}', 'UsersController@update');
Route::get('/reference/provide/{id}', 'ReferenceController@provide')->middleware('auth');
Route::post('/reference/update/{id}', 'ReferenceController@update');
Route::post('/reference/createrecord/{id}', 'ReferenceController@createrecord');
Route::get('/qr/generate/{id}', 'QRController@generate');
Route::get('/search/option/', 'ReferenceController@option');
Route::get('/search/searchbyid/', 'ReferenceController@searchbyid');
Route::get('/search/searchbyqr/', 'ReferenceController@searchbyqr');
Route::post('/search/searchforid', 'ReferenceController@searchforid');
Route::post('/search/result_searchbyid/{id}', 'ReferenceController@result_searchbyid')->middleware('auth');
Route::get('/search/result_searchbyid1/{id}', 'ReferenceController@result_searchbyid1')->middleware('auth');
Route::get('/search/option_ask/', 'ReferenceController@option_ask');
Route::get('/search/searchbyid_ask/', 'ReferenceController@searchbyid_ask');
Route::get('/search/searchbyqr_ask/', 'ReferenceController@searchbyqr_ask');
Route::post('/search/result_searchbyid_ask/{id}', 'ReferenceController@result_searchbyid_ask')->middleware('auth');
Route::get('/search/result_searchbyid1_ask/{id}', 'ReferenceController@result_searchbyid1_ask')->middleware('auth');
Route::get('/reference/ask/{id}', 'ReferenceController@ask')->middleware('auth');
Route::post('/reference/createrecord_ask/{id}', 'ReferenceController@createrecord_ask');
Route::get('/reference/myreferences/{id}', 'ReferenceController@myreferences');
Route::get('/reference/someonereferences/{id}', 'ReferenceController@someonereferences');
Route::get('/reference/answer/{id}/{reference_id}', 'ReferenceController@answer');
Route::post('/reference/answer_update/{id}', 'ReferenceController@answer_update');
Route::get('/reference/approval', 'ReferenceController@approval');
Route::get('/reference/approve/{id}', 'ReferenceController@approve');
Route::get('/cv/{name}', 'ReferenceController@cv');

//Route::get('/', function () {return view('home');})->middleware('auth');
//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/admin', 'UsersController@admin');
//Route::resource('users','UsersController');
//Route::get('/users/{id}/{name}', function ($id, $name) {
//    return 'ID: '.$id.' Name: '.$name;
//});

//Route::post('/search/result_searchbyid/{id}', 'ReferenceController@result_searchbyid');

/*
Route::get('/search/result_searchbyid/', function () {
    print_r('GG');
});
 */

//Route::get('/', 'ReferenceController@getData');

/*

Route::get('/register', function(){
    $Country = Country::all();
    return view('auth.register')->with('Country',$Country);
});
*/
//Route::get('/register', function () {
//    return view('auth.register');
//});

//Route::get('auth.index', 'auth.RegisterController@index');
//Route::get('auth.register', 'auth.RegisterController@showCountry');



