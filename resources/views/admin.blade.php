@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-2">
            <div class="card">

                <div class="user-panel p-3  mb-2">

                    <div class="float-left image">
                        <img class="user_avatar" src="assets/img/dummy/{{$user->gender_id}}.png" alt="User Image">
                    </div>

                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">{{$user->name}}</h6>
                        <h6 class="font-weight-light mt-2 mb-1">{{$user->email}}</h6>
                        <h6 class="font-weight-light mt-2 mb-1 icon-circle text-primary blink">Online</h6>
                    </div>

                </div>

                <ul class="list-group ">

                    <a href="/qr/generate/{{$id}}"><li  class="list-group-item list-group-item-action">
                            <i class="icon icon-qrcode text-green"></i>Generate QR Code
                        </li></a>



                <ul class="list-group ">
                    <a href="{{ route('logout') }}" ><li  class="list-group-item list-group-item-action">
                            <i class="icon icon-exit_to_app text-black"></i>Logout
                        </li></a>
                </ul>

            </div>
        </div>
    </div>
</div>

@endsection
