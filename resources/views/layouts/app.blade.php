<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('includes.styles')
</head>
<body>
        <main class="py-4 ">
            @include('includes.header')

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-2">
                            @include('includes.messages')
                    </div>
                </div>
            </div>

            @yield('content')
            @include('includes.footer')
            @include('includes.javascript')
        </main>
</body>
</html>