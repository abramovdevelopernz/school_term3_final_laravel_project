@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-2">
            <div class="card">

                <div class="user-panel p-3  mb-2">

                            <div class="float-left image">
                                <img class="user_avatar" src="assets/img/dummy/{{$user->gender_id}}.png" alt="User Image">
                            </div>
                            <div class="float-left info">
                                <h6 class="font-weight-light mt-2 mb-1">{{$user->name}}</h6>
                                <h6 class="font-weight-light mt-2 mb-1">{{$user->email}}</h6>
                                <h6 class="font-weight-light mt-2 mb-1 icon-circle text-primary blink">Online</h6>
                            </div>

                 </div>

<ul class="list-group ">

    @if ($admin==0)

    <a href="/qr/generate/{{$id}}"><li  class="list-group-item list-group-item-action">
            <i class="icon icon-qrcode text-green"></i>Generate QR Code
     </li></a>

    <a href="/user/show/{{$id}}" ><li  class="list-group-item list-group-item-action">
        <i  class="icon icon-drivers-license text-blue"></i>Profile
    </li></a>

    <a href="/user/edit/{{$id}}" ><li  class="list-group-item list-group-item-action">
            <i class="icon icon-document-edit text-blue "></i>Edit Profile
    </li></a>

    <a href="/search/option/" ><li  class="list-group-item list-group-item-action">
            <i class="icon icon-handshake text-yellow"></i>Provide a reference
    </li></a>


    <a href="/search/option_ask/" ><li  class="list-group-item list-group-item-action">
            <i class="icon icon-transform text-red"></i>Ask for reference
    </li></a>

</ul>

<ul class="list-group ">
        <a href="/reference/myreferences/{{$id}}" ><li  class="list-group-item list-group-item-action">
            <i class="icon icon-hand-o-right text-green"></i>My references
        </li></a>

        <a href="/reference/someonereferences/{{$id}}" ><li  class="list-group-item list-group-item-action">
            <i  class="icon icon-hand-o-left text-blue"></i>Someone asked me fo reference
        </li></a>
</ul>

                @endif


                @if ($admin==1)

                <ul class="list-group ">
                    <a href="/reference/approval/" ><li  class="list-group-item list-group-item-action">
                            <i class="icon icon-hand-o-right text-green"></i>Reference Approval
                        </li></a>

                    <a href="/clear" ><li  class="list-group-item list-group-item-action">
                            <i  class="icon icon-hand-o-left text-blue"></i>Service
                        </li></a>
                </ul>





                @endif


<ul class="list-group ">
        <a href="{{ route('logout') }}" ><li  class="list-group-item list-group-item-action">
            <i class="icon icon-exit_to_app text-black"></i>Logout
        </li></a>
</ul>

            </div>
        </div>
    </div>
</div>

@endsection
