@extends('layouts.app')

@section('content')

<div class="container-fluid animatedParent animateOnce my-3  ">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">

            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row ">
                    <div class="col-md-3 ">
                        <div class="card ">
                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Your QR code</strong>
                        </div>

                        <p class="fig">
                            <img  src="\qr.png" width="200" height="200" alt="Italian Trulli">
                        <p>

                            <a href="/home" ><li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-exit_to_app text-black"></i>Home
                                </li>
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
</div>

@endsection

