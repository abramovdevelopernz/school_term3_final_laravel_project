@extends('layouts.app')
@section('content')




{!! Form::open(['action' => ['ReferenceController@result_searchbyid',0],'method' => 'POST']) !!}

<div class="container-fluid animatedParent animateOnce my-3">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">

                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Search by ID</strong>
                        </div>


                        <ul class="list-group list-group-flush">

                            <li class="list-group-item"><i class="icon icon-face-stuck-out-tongue text-primary"></i><strong class="s-12">Person ID</strong>
                                <span class="float-right s-12">

                                     {{Form::text('searchid','',['class'=>'form-control r-0 light s-12','placeholder'=>''])}}
                                </span>
                            </li>



                            <a href="/home">
                                <li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-exit_to_app text-black"></i>Back
                                </li>
                            </a>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    {{Form::submit('Search',['class'=>'btn btn-primary'])}}



</div>

</div>





{!! Form::close() !!}
@endsection























{{--


{!! Form::open(['action' => ['ReferenceController@result_searchbyid',0],'method' => 'POST']) !!}


<div class="form-group">
    {{Form::label('name','Name')}}
    {{Form::text('searchid','',['class'=>'form-control','placeholder'=>'searchid'])}}
</div>

{{Form::submit('Search',['class'=>'btn btn-primary'])}}

<a class="btn btn-link" href="/home">{{ __('Back') }}</a>

{!! Form::close() !!}
@endsection
--}}

