@extends('layouts.app')
@section('content')

<script src="{{ asset('js/instascan.min.js') }}"></script>
<script src="{{ asset('js/appscan.js') }}" defer></script>

<div class="container-fluid animatedParent animateOnce my-3">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Search by QR</strong>
                        </div>

                        <video id="preview"></video>

                        <a href="/search/option/" ><li  class="list-group-item list-group-item-action">
                                <i class="icon icon-exit_to_app text-black"></i>Back
                            </li></a>


                        <a href="/home" ><li  class="list-group-item list-group-item-action">
                                <i class="icon icon-exit_to_app text-black"></i>Home
                            </li></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<script type="text/javascript">
      let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      scanner.addListener('scan', function (content) {
      window.location.replace("http://localhost:8000/search/result_searchbyid1/"+content);
    //alert(content);
      });

      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
          scanner.start(cameras[0]);
        } else {
          console.error('No cameras found.');
        }
      }).catch(function (e) {
        console.error(e);
      });
</script>
@endsection