@extends('layouts.app')
@section('content')

<div class="container-fluid animatedParent animateOnce my-3 ">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">

                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Look for person</strong>
                        </div>


                        <ul class="list-group list-group-flush">

                            <a href="/search/searchbyid_ask/" >
                                <li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-play_circle_filled text-primary"></i>Search by ID
                                </li>
                            </a>


                            <a href="/search/searchbyqr_ask/" >
                                <li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-play_circle_filled text-success"></i>Search by QR Code
                                </li>
                            </a>

                            <a href="/home" >
                                <li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-exit_to_app text-black"></i>Back
                                </li>
                            </a>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



