@extends('layouts.app')
@section('content')


{!! Form::open(['action' => ['UsersController@update',$user->id],'method' => 'POST']) !!}

<div class="container-fluid animatedParent animateOnce my-3">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">

                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Information</strong>
                        </div>


                        <ul class="list-group list-group-flush">

                            <li class="list-group-item"><i class="icon icon-person_outline text-primary"></i><strong class="s-12">Name</strong>
                                <span class="float-right s-12">

                                     {{Form::text('name',$user->name,['class'=>'form-control r-0 light s-12','placeholder'=>'Name'])}}
                                </span>
                            </li>

                            <li class="list-group-item"><i class="icon icon-email text-success"></i><strong class="s-12">Email</strong>
                                <span class="float-right s-12">

                                     {{Form::text('email',$user->email,['class'=>'form-control r-0 light s-12','placeholder'=>'Email'])}}
                                </span>
                            </li>


                            <li class="list-group-item"><i class="icon icon-phone2 text-warning"></i><strong class="s-12">Phone</strong>
                                <span class="float-right s-12">
                                {{Form::text('phone',$user->phone,['class'=>'form-control r-0 light s-12','placeholder'=>'Phone'])}}
                                </span>
                            </li>

                            <li class="list-group-item"><i class="icon icon-location_city text-danger"></i><strong class="s-12">City</strong>
                                <span class="float-right s-12">
                                {{Form::text('city',$user->city,['class'=>'form-control r-0 light s-12','placeholder'=>'City'])}}
                                </span>
                            </li>


                            <li class="list-group-item"><i class="icon icon-hospital-o text-success"></i><strong class="s-12">Country</strong>
                                <span class="float-right s-12">
                                    <select name="country_id" class="custom-select my-1 mr-sm-2 form-control r-0 light s-12">
                                        @foreach($Country as $Country)
                                        <option value="{{$Country['id']}}" {{ ( $Country->id == $user->country_id   ) ? 'selected' : '' }}> {{$Country['name']}}</option>
                                        @endforeach
                                    </select>
                                </span>
                            </li>

                            <li class="list-group-item"><i class="icon icon-signal_cellular_4_bar text-primary"></i><strong class="s-12">Experience</strong>
                                <span class="float-right s-12">

                                      @if($user->experience =='0')
                                        <input type="checkbox" name="experience" class="form-check-input" value="0">
                                        @else
                                        <input type="checkbox" name="experience" class="form-check-input" value="1" checked>
                                      @endif


                                </span>
                            </li>



                            <li class="list-group-item"><i class="icon icon-mars-double text-warning"></i> <strong class="s-12">Gender</strong>
                                <span class="float-right s-12">
                                    @if($user->gender_id =='2')
                                            <input type="radio" name="gender_id" value="2" checked>Male<br>
                                            <input type="radio" name="gender_id" value="3" >Female<br>
                                        @else
                                            <input type="radio" name="gender_id" value="2" >Male<br>
                                            <input type="radio" name="gender_id" value="3" checked>Female<br>
                                    @endif
                                </span>
                            </li>


                            <li class="list-group-item"><i class="icon icon-document-certificate  text-danger"></i><strong class="s-12">Visa</strong>
                                <span class="float-right s-12">
                                    <select name="visa_id" class="custom-select my-1 mr-sm-2 form-control r-0 light s-12">
                                        @foreach($Visa as $Visa)
                                        <option value="{{$Visa['id']}}" {{ ( $Visa->id == $user->visa_id   ) ? 'selected' : '' }}> {{$Visa['name']}}</option>
                                        @endforeach
                                    </select>
                                </span>
                            </li>

                            <a href="/home">
                                <li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-exit_to_app text-black"></i>Back
                                </li>
                            </a>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<br>
    {{Form::submit('Update',['class'=>'btn btn-primary'])}}

</div>
</div>

{!! Form::close() !!}
@endsection








