@extends('layouts.app')

@section('content')

@forelse($user as $user)

<div class="container-fluid animatedParent animateOnce my-3 ">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">

                            <div class="card-header bg-white fig"">
                                <strong class="card-title">Your profile</strong>
                            </div>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><i class="icon icon-person_outline text-primary"></i><strong class="s-12">Name</strong> <span class="float-right s-12">{{$user->name}}</span></li>
                                <li class="list-group-item"><i class="icon icon-email text-success"></i><strong class="s-12">Email</strong> <span class="float-right s-12">{{$user->email}}</span></li>
                                <li class="list-group-item"><i class="icon icon-phone2 text-warning"></i><strong class="s-12">Phone</strong> <span class="float-right s-12">{{$user->phone}}</span></li>
                                <li class="list-group-item"><i class="icon icon-location_city text-danger"></i><strong class="s-12">City</strong> <span class="float-right s-12">{{$user->city}}</span></li>

                                @if($user->experience =='0')
                                <li class="list-group-item"><i class="icon icon-signal_cellular_4_bar text-primary"></i> <strong class="s-12">Previous experience</strong> <span class="float-right s-12">Yes</span></li>
                                @else
                                <li class="list-group-item"><i class="icon icon-signal_cellular_4_bar text-primary"></i> <strong class="s-12">Previous experience</strong> <span class="float-right s-12">No</span></li>
                                @endif

                                <li class="list-group-item"><i class="icon icon-hospital-o text-success"></i> <strong class="s-12">Country</strong> <span class="float-right s-12">{{$user->country_name}}</span></li>
                                <li class="list-group-item"><i class="icon icon-mars-double text-warning"></i> <strong class="s-12">Gender</strong> <span class="float-right s-12">{{$user->gender_name}}</span></li>
                                <li class="list-group-item"><i class="icon icon-document-certificate  text-danger"></i> <strong class="s-12">Visa</strong> <span class="float-right s-12">{{$user->visa_name}}</span></li>
                                <a href="/home" ><li  class="list-group-item list-group-item-action">
                                        <i class="icon icon-exit_to_app text-black"></i>Back
                                    </li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

@empty

<div class="container-fluid animatedParent animateOnce my-3">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Information</strong>
                        </div>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><i class="icon icon-web text-danger"></i> <strong class="s-12"></strong> <span class="float-right s-12">No information</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <a href="/home" class="btn btn-primary">Back</a>

@endforelse

@endsection
