@extends('layouts.app');

@section('content')
<h1>Country</h1>



<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
    <label for="country_id" class="col-md-4 control-label">Country</label>
    <div class="col-md-6">
        <select class="form-control">
            @foreach($Country as $Country)
            <option value=" {{ $Country['id'] }} " name="country_id">{{ $Country['name'] }}</option>
            @endforeach
        </select>
        @if ($errors->has('gender'))
        <span class="help-block">
         <strong>{{ $errors->first('gender') }}</strong>
         </span>
        @endif
    </div>
</div>


@endsection