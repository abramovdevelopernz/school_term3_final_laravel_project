@extends('layouts.app')
@section('content')

<div class="container-fluid animatedParent animateOnce my-3 ">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="card my-3 no-b ">
                <div class="card-header white b-0 p-3">
                    <h4 class="card-title">Reference approval</h4>
                </div>
                <a href="/home">
                    <li  class="list-group-item list-group-item-action">
                        <i class="icon icon-exit_to_app text-black"></i>Back
                    </li>
                </a>
                <div class="collapse show" id="salesCard" style="">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-hover earning-box">
                                <tbody>
                                @if(count($reference)>0)
                                @foreach($reference as $reference)
                                <tr>
                                    <td class="w-10">
                                        <a href="panel-page-profile.html" class="avatar avatar-lg">
                                            @if($reference->gender_id =='2')
                                            <img src="  {{ asset('assets/img/dummy/2.png') }}    " alt="">
                                            @endif

                                            @if($reference->gender_id =='3')
                                            <img src="  {{ asset('assets/img/dummy/3.png') }}    " alt="">
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <h6>Reference is given to: {{$reference->who_ask_name}}</h6>


                                        <small>Reference Type: {{$reference->reftype}}</small> <br>
                                        <small>Position: {{$reference->position}}</small><br>
                                        <small>Date of ask: {{$reference->date_of_ask}}</small><br>
                                        <small>Date of reference: {{$reference->date_of_reference}}</small><br>
                                        <small>Reference notes: {{$reference->reference_notes}}</small><br>
                                        <small>Rank: {{$reference->rank_id}}</small><br>
                                        <small>Status: {{$reference->status}}</small><br>

                                        @if($reference->approved_tf =='0')
                                        <small>Approved: No</small>
                                        @else
                                        <small>Approved: Yes</small>
                                        @endif
                                        <br><b>Reference is asked from: {{$reference->who_give_name}}</b>


                                        <br>
                                        @if($reference->approved_tf =='0')
                                        <a href="/reference/approve/{{$reference->id}}" type="button" class="btn btn-success btn-xs">Approve</a>
                                        @else

                                        @endif


                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <a href="/home">
                    <li  class="list-group-item list-group-item-action">
                        <i class="icon icon-exit_to_app text-black"></i>Back
                    </li>
                </a>
            </div>
        </div>
    </div>
</div>

@else
<p>No data found</p>
@endif

@endsection