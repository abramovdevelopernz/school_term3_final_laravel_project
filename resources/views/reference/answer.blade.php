@extends('layouts.app')
@section('content')

{!! Form::open(['action' => ['ReferenceController@answer_update',0],'method' => 'POST']) !!}
<input type="hidden" id="reference_id"  name="reference_id"  value="{{$id}}">
<div class="container-fluid animatedParent animateOnce my-3">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">

                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Answer</strong>
                        </div>

                            <li class="list-group-item"><i class="icon icon-sticky-note-o text-success"></i><strong class="s-12">Notes</strong>
                                <span class="float-right s-12">

                                     {{Form::text('reference_notes','',['class'=>'form-control r-0 light s-12','placeholder'=>''])}}
                                </span>
                            </li>

                            <li class="list-group-item"><i class="icon icon-star2 text-success"></i><strong class="s-12">Rank</strong>
                                <span class="float-right s-12">
                                    <input type="radio" name="rank_id" value="1" checked>1 star<br>
                                    <input type="radio" name="rank_id" value="2">2 star<br>
                                    <input type="radio" name="rank_id" value="3">3 star<br>
                                    <input type="radio" name="rank_id" value="4">4 star<br>
                                    <input type="radio" name="rank_id" value="5">5 star<br>
                                </span>
                            </li>

                        <a href="/home" ><li  class="list-group-item list-group-item-action">
                                <i class="icon icon-exit_to_app text-black"></i>Home
                            </li>
                        </a>

                        <a href="/reference/someonereferences/{{$reference_id}}" ><li  class="list-group-item list-group-item-action">
                                <i class="icon icon-exit_to_app text-black"></i>Back
                            </li>
                        </a>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    {{Form::submit('OK',['class'=>'btn btn-primary'])}}
</div>
</div>

{!! Form::close() !!}
@endsection