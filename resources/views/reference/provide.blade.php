@extends('layouts.app')
@section('content')

{!! Form::open(['action' => ['ReferenceController@createrecord',$user_id],'method' => 'POST']) !!}
<input type="hidden" id="who_gives_user_id" name="who_gives_user_id" value="{{$user_id}}">
<input type="hidden" id="who_asks_user_id"  name="who_asks_user_id"  value="{{$id}}">
<div class="container-fluid animatedParent animateOnce my-3">
    <div class="animated fadeInUpShort go">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card ">

                            <div class="card-header bg-white fig"">
                            <strong class="card-title">Provide a reference</strong>
                        </div>


                                <div class="table-responsive">
                                    <table class="table table-hover earning-box">
                                        <thead class="no-b">
                                        <tr>
                                            <th colspan="2">Person Name</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @forelse($user as $user)

                                        <tr>
                                            <td class="w-10">
                                                <a href="panel-page-profile.html" class="avatar avatar-lg">

                                                    @if($user->gender_id =='2')
                                                    <img src="  {{ asset('assets/img/dummy/2.png') }}    " alt="">
                                                    @endif

                                                    @if($user->gender_id =='3')
                                                    <img src="  {{ asset('assets/img/dummy/3.png') }}    " alt="">
                                                    @endif

                                                </a>
                                            </td>

                                            <td>
                                                <h6>{{$user->name}}</h6>
                                                <small class="text-muted">{{$user->email}}</small>
                                            </td>


                                        </tr>


                                        @empty
                                        <p>No Comments!</p>
                                        @endforelse


                                        </tbody>
                                    </table>
                                </div>
                            <div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.95) none repeat scroll 0% 0%; width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 238.688px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                        <ul class="list-group list-group-flush">

                            <li class="list-group-item"><i class="icon icon-share2 text-primary"></i>
                                <strong class="s-12">Type</strong>
                                    <span class="float-right s-12">



                                        <select onchange="myFunction()" name="reftype_id" id="reftype_id" class="custom-select my-1 mr-sm-2 form-control r-0 light s-12">
                                            @foreach($Reftype as $Reftype)
                                            <option value="{{$Reftype['id']}}" > {{$Reftype['name']}}</option>
                                            @endforeach
                                        </select>
                                    </span>
                            </li>
                            <li class="list-group-item" id = "position" style="display: none;"><i class="icon icon-snapchat-ghost text-success"></i><strong class="s-12">Position</strong>
                                <span class="float-right s-12">

                                     {{Form::text('position','',['class'=>'form-control r-0 light s-12','placeholder'=>''])}}
                                </span>
                            </li>
                            <li class="list-group-item"><i class="icon icon-sticky-note-o text-warning"></i><strong class="s-12">Notes</strong>
                                <span class="float-right s-12">

                                     {{Form::text('reference_notes','',['class'=>'form-control r-0 light s-12','placeholder'=>''])}}
                                </span>
                            </li>
                            <li class="list-group-item"><i class="icon icon-star_border text-danger"></i><strong class="s-12">Rank</strong>
                                <span class="float-right s-12">
                                    <input type="radio" name="rank_id" value="1" checked>1 star<br>
                                    <input type="radio" name="rank_id" value="2">2 star<br>
                                    <input type="radio" name="rank_id" value="3">3 star<br>
                                    <input type="radio" name="rank_id" value="4">4 star<br>
                                    <input type="radio" name="rank_id" value="5">5 star<br>
                                </span>
                            </li>

                            <a href="/home">
                                <li  class="list-group-item list-group-item-action">
                                    <i class="icon icon-exit_to_app text-black"></i>Back
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    {{Form::submit('Create',['class'=>'btn btn-primary'])}}
</div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
function myFunction() {
var select_status = $('#reftype_id').val();

if (select_status == '3'){
$('#position').show();
} else
{
$('#position').hide();
}

}
</script>

@endsection