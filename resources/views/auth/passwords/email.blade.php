@extends('layouts.app')

@section('content')


<div id="primary" class="p-t-b-0 height-full ">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mx-md-auto">

                <div class="text-center">
                    <img src="assets/img/dummy/u5.png" alt="">
                    <h3 class="mt-2">Reset password</h3>

                </div>

                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif


                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group has-icon focused"><i class="icon-envelope-o"></i>

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                    </div>

                    <input type="submit" class="btn btn-success btn-lg btn-block" value="Send Password Reset Link">
                </form>

                <br>

                <a href="{{ route('login') }}"  class="btn btn-success btn-lg btn-block ">Login</a>


            </div>
        </div>
    </div>
</div>

@endsection

<!--

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password1') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('login') }}">
                                    {{ __('Login') }}
                                </a>




                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

--!>


