@extends('layouts.app')

@section('content')

<div id="primary" class="p-t-b-0 height-full ">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mx-md-auto">

                <div class="text-center">
                    <img src="assets/img/dummy/logos/v7.png" alt="">
                    <h3 class="mt-2">NZ reference service</h3>
                    <p class="p-t-b-20">Hi all , this web application was made as a final school project by Alex and Gabriela
                        for you
                    </p>
                </div>

                <form method="POST" action="{{ route('login') }}">

                    @csrf

                    <div class="form-group has-icon focused"><i class="icon-envelope-o"></i>
                    <input type="text" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }} " id="email" placeholder="Email Address"  name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif

                    </div>
                    <div class="form-group has-icon focused"><i class="icon-user-secret"></i>

                        <input id="password" type="password" class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>


                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>

                    <input type="submit" class="btn btn-success btn-lg btn-block" value="Log In">
                </form>

                <br>

                <a href="{{ route('register') }}"  class="btn btn-success btn-lg btn-block ">Register</a>
                <a href="{{ route('password.request') }}"  class="btn btn-success btn-lg btn-block ">Forgot Your Password?</a>

                <a href="http://127.0.0.1:8000/cv/g@gmail.com"  class="btn btn-success btn-lg btn-block ">Check references</a>



        </div>
        </div>
    </div>
</div>














<!--

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('register') }}">
                                    {{ __('Register') }}
                                </a>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
--!>

@endsection
