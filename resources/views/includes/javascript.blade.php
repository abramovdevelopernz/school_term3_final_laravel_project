{{--
<script src="{{ asset('js/jquery-3.3.1.min.js') }}" defer></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/my.js') }}" ></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
--}}

<script src="{{ asset('/assets/js/app.js') }}"></script>
<script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>
@stack('script')